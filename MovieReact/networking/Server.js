import React, {Component} from 'react';
import {
  AppRegistry,
  SectionList,
  StyleSheet,
  Alert,
  Platform,
  Text,
  View,
} from 'react-native';

// Core API
const BaseURL = 'http://training-movie.bsp.vn:82/';
const AppToken = 'dCuW7UQMbdvpcBDfzolAOSGFIcAec11a';

//URL STRING
const stringFinalUrl = {
  getVideo: 'movie/list?',
  registry: 'user/registry',
  login: 'user/login',
  forPass: 'user/forgot-password',
  logout:'user/logout',
  loginSocial: 'user/social-login'
};

//METHOD
const Method = {
  post : 'POST',
  get :'GET',
};

async function getVideoFromServer(crPage) {
  try {
    let pageString =`page=${crPage}`

    const apiStringURl = BaseURL + stringFinalUrl.getVideo + pageString ;
    console.log(`get Video FromServer  => : ${apiStringURl} `);
    const requestOptions = {
      method: Method.get,
      headers: {app_token: AppToken},
    };
    let response = await fetch(apiStringURl, requestOptions);
    let responseJson = await response.json();
    // console.log('getVideoFromServer', JSON.stringify(responseJson));
    return responseJson;
  } catch (error) {
    console.log(`Error is getVideoFromServer: ${error} `);
  }
}

async function registryFromServer(par) {
  let fromdata = new FormData()
  fromdata.append('email',par.email)
  fromdata.append('password',par.password)
  fromdata.append('full_name',par.UserName)
  try {
    const apiStringURl = BaseURL + stringFinalUrl.registry;
    const requestOptions = {
      method: Method.post,
      headers: {app_token: AppToken, 'Content-Type': 'application/json'},
      body: fromdata,
    };
    let response = await fetch(apiStringURl, requestOptions);
    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.log(`Error is registryFromServer: ${error} `);
  }
}

// login
async function loginFromServer(params) {

let fromdata = new FormData()
fromdata.append('email', 'a99@gmail.com')//params.email)
fromdata.append('password', '123456')//params.password)

  console.log('login server', params)
  try {
    const apiStringURl = BaseURL + stringFinalUrl.login;
    const requestOptions = {
      method:  Method.post,
      headers: {app_token: AppToken, 'Content-Type': 'application/json'},
      // body: JSON.stringify(params),
      body: fromdata
    };
    let response = await fetch(apiStringURl, requestOptions);
    let responseJson = await response.json();
    console.log('login server data call', responseJson)
    return responseJson;
  } catch (error) {
    console.log(`Error is loginFromServer: ${error} `);
  }
}

// login
async function loginFromServerSocial(params) {

  let fromdata = new FormData()
  fromdata.append('email','duong@gmail.com')
  fromdata.append('full_name','fasdfs')
  fromdata.append('google_id','params.googleID')
  
    console.log(' loginFromServerSocial', fromdata)
    try {
      const apiStringURl = BaseURL + stringFinalUrl.loginSocial;
      const requestOptions = {
        method:  Method.post,
        headers: {app_token: AppToken, 'Content-Type': 'application/json'},
        body: fromdata
      };
      let response = await fetch(apiStringURl, requestOptions);
      let responseJson = await response.json();
      console.log('loginFromServerSocial call data', responseJson)
      return responseJson;
    } catch (error) {
      console.log(`Error is loginFromServerSocial: ${error} `);
    }
  }


// ForPass
async function forPassFromServer(params) {
    try {
      let pageString =`?email=${params}`
      const apiStringURl = BaseURL + stringFinalUrl.forPass+pageString;
      const requestOptions = {
        method:  Method.post,
        headers: {app_token: AppToken, 
          'Content-Type': 'application/json'},
      };
      console.log('forPassFromServer server url', apiStringURl)
      let response = await fetch(apiStringURl, requestOptions);
      let responseJson = await response.json();
      return responseJson;
    } catch (error) {
      console.log(`Error is forPass FromServer: ${error}`);
    }
  
}
// 
async function logOutFromServer(params) {  
    console.log('login server', params)
    try {
      let fromdata = new FormData()
      fromdata.append('email',params.email)
      const apiStringURl = BaseURL + stringFinalUrl.logout;
      const requestOptions = {
        method:  Method.post,
        headers: {app_token: AppToken, 
          access_token: params,
          'Content-Type': 'application/json'},
          body: fromdata,
      };
      let response = await fetch(apiStringURl, requestOptions);
      let responseJson = await response.json();
      console.log('logOut server data call', responseJson)
      return responseJson;
    } catch (error) {
      console.log(`Error is logOut FromServer: ${error} `);
    }
}

export {getVideoFromServer};
export {registryFromServer};
export {loginFromServer};
export {forPassFromServer};
export {logOutFromServer};
export {loginFromServerSocial};
