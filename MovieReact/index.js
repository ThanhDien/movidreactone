/**
 * @format
 */

import {AppRegistry, Platform} from 'react-native';
import {name as appName} from './app.json';
import createAppContainer from './src/screens/appStart';
import PushNotification from 'react-native-push-notification';


import {
  Home,
  Login,
  Registered,
  DetailMovie,
  Forgotpassword,
} from './ScreensName';


PushNotification.configure({
  
  onRegister: function (token) {
    console.log("TOKEN:", token);
  },
  
  onNotification: function (notification) {
    console.log("NOTIFICATION:", notification);
  },

  
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  popInitialNotification: true,
  requestPermissions: Platform.OS==='ios',
});

AppRegistry.registerComponent(appName, () => createAppContainer);
