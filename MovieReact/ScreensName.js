const LoginScreen = 'LoginComponent';
const HomeScreen = 'HomeComponent';
const ForgotpasswordScreen = 'ForgotpasswordComponent';
const RegisteredScreen = 'RegisteredComponent';
const DetailMovieScreen = 'DetailMovieComponent';
const MapGGScreen ='MapGGComponent';
const TestPushIosScreen ='TestPushIosComponent';

export { LoginScreen, HomeScreen, ForgotpasswordScreen, RegisteredScreen, DetailMovieScreen,MapGGScreen,TestPushIosScreen };
 