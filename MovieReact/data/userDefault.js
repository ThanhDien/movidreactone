import AsyncStorage from "@react-native-async-storage/async-storage"



const setStoreDataUser  = async (key,value) => {
    try {
      await AsyncStorage.setItem(key, value)
    } catch (e) {
      // saving error
    }
  }
  
const getDataUser = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key)
      if(value !== null) {
        return value;
      }
    } catch(e) {
      // error reading value
    }
  }
  
// CheckLogin 
  const getISLOGIN = async (key) => {
    try {
      const value = JSON.parse(await AsyncStorage.getItem(key)) 
      if(value !== null) {
        return value;
      }
    } catch(e) {
      // error reading value
    }
  }

 // logOut
const getLOGOut = async (key) =>  {
    try {
        await AsyncStorage.removeItem(key);
        return true;
    }
    catch(exception) {
        return false;
    }
  }
  


  export {setStoreDataUser} ;
  export {getDataUser} ;
  export {getISLOGIN} ;
  export {getLOGOut} ;