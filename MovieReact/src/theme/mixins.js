let normalize = (color) => {
  let re = [1, 0, 0, 0];
  if (color[0] == '#') { //hexa
    let value = color.substring(1);
    if (value.length == 3) {
      let arr = value.split('');
      arr.forEach((v, i) => re[i+1] = parseInt(v + v, 16));
    } else if (value.length == 6) {
      let arr = value.match(/.{2}/g);
      arr.forEach((v, i) => re[i+1] = parseInt(v, 16));
    }
    return re;
  } else if (color.indexOf('rgb') >= 0) { //rgb() rgba()
    let open = color.indexOf('(');
    let close = color.indexOf(')');
    let value = color.substring(open + 1, close).split(',');
    if (value.length == 4) {
      re[0] = parseFloat(value[3])
      if (re[0] > 1) {
        re[0] = re[0]/255.0;
      }
      value.pop();
    }
    value.forEach((v, i) => re[i+1] = parseInt(v));
    return re;
  }
}

let Array2Hexa = (arr) => {
  let fixHexaChunk = (v) => v.length == 1 ? `0${v}` : v;
  arr = arr.slice();
  arr.shift();
  return '#' + arr.map(v => fixHexaChunk(v.toString(16))).join('');
}

let Array2Rgb = (arr) => {
  arr = arr.slice();
  let alpha = arr[0];
  arr.shift();
  if (alpha < 1) {
    return `rgba(${arr.map(v => v).join(',')}, ${alpha})`;
  } else {
    return `rgb(${arr.map(v => v).join(',')})`;
  }
}

let hsl = (color) => {
  let arr = normalize(color);
  let alpha = arr[0];
  arr.shift();
  var max = Math.max(...arr);
  var min = Math.min(...arr);
  var h, s, l = ((max + min) / 255.0) / 2;

  if (max == min){
    h = s = 0; // achromatic
  } else {
    let _max = max/255.0;
    let _min = min/255.0;
    var d = _max - _min;
    s = l > 0.5 ? d / (2 - _max - _min) : d / (_max + _min);
    let r = arr[0]/255.0, g = arr[1]/255.0, b = arr[2]/255.0;
    switch(max){
      case arr[0]: h = (g - b) / d + (g < b ? 6 : 0); break;
      case arr[1]: h = (b - r) / d + 2; break;
      case arr[2]: h = (r - g) / d + 4; break;
    }
    h /= 6;
  }
  return [h, s, l, alpha];
}

let hsl2Array = (hsl) => {
  let r, g, b;
  let h = hsl[0], s = hsl[1], l = hsl[2];
  if(s == 0){
      r = g = b = l; // achromatic
  } else {
    let hue2rgb = (p, q, t) => {
      if(t < 0) t += 1;
      if(t > 1) t -= 1;
      if(t < 1/6) return p + (q - p) * 6 * t;
      if(t < 1/2) return q;
      if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
      return p;
    }
    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }
  return [hsl[3], Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

let darken = (color, amount) => {
  let _hsl = hsl(color);
  if (typeof(amount) == 'string') {
    amount = amount.trim();
    if (amount.indexOf('%') >= 0) {
      amount = parseFloat(amount.substring(0, amount.length-1))/100.0;
    }
  }
  _hsl[2] = Math.max(_hsl[2] - amount, 0);
  let re = Array2Rgb(hsl2Array(_hsl));
  return re;
}

let lighten = (color, amount) => {
  let _hsl = hsl(color);
  if (typeof(amount) == 'string') {
    amount = amount.trim();
    if (amount.indexOf('%') >= 0) {
      amount = parseFloat(amount.substring(0, amount.length-1))/100.0;
    }
  }
  _hsl[2] = Math.min(_hsl[2] + amount, 1);
  let re = Array2Rgb(hsl2Array(_hsl));
  return re;
}

let alpha = (color, alpha) => {
  let tmp = normalize(color);
  tmp[0] = alpha;
  return Array2Rgb(tmp); 
}

let inverse = (color) => {
  let arr = normalize(color);
  let c = arr[0] * 255;
  arr[1] = c - arr[1];
  arr[2] = c - arr[2];
  arr[3] = c - arr[3];
}

let emphasis = (color) => darken(color, '10%');

let larger = (size, percent) => size + (size * percent)
let smaller = (size, percent) => size - (size * percent)
let largerInt = (size, percent) => Math.round(size + (size * percent))
let smallerInt = (size, percent) => Math.round(size - (size * percent))

export default {
  darken: darken,
  lighten: lighten,
  inverse: inverse,
  larger: larger,
  smaller: smaller,
  largerInt: largerInt,
  smallerInt: smallerInt,
  alpha: alpha
}