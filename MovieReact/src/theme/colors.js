

const COLORS = {
  Orange: '#ff8000',
  White: '#fff',
  Gray: 'gay',

  facebook: '#3b5998',
  transparent: 'rgba(0,0,0,0)',
  silver: '#F7F7F7',
  steel: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
}

export {COLORS}
