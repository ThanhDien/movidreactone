import {Alert} from 'react-native';



export const showAlert = (title, body) => {
    Alert.alert(
      title, body,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: true },
    );
}

export const validateEmail = (text) => {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
  if (reg.test(text) === false) {
      return false;
  } else {
    return true
  }
}

export const getParamsFromUrl = (url) => {
  let regex = /[?&]([^=#]+)=([^&#]*)/g,
    params = {},
    match;
  while (match = regex.exec(url)) {
    params[match[1]] = match[2];
  }
  return params
}

export const getCurrentTimeWithGMTOffset = (offset) => {

  let d = new Date();

  // convert to msec
  // add local time zone offset
  // get UTC time in msec
  let utc = d.getTime() + (d.getTimezoneOffset() * 60000);

  // create new Date object for different city
  // using supplied offset
  let convertedDate = new Date(utc + (3600000*offset));

  // return convertedDate.toLocaleString();
  return convertedDate;
}



export function isStringNullOrEmpty(variable) {
  return (null === variable || undefined === variable || '' === variable || 'null' === variable || 'undefined' === variable);
}

export function isNull(variable) {
  return (null === variable || undefined === variable);
}

export function formatStringNumber(value) {
  let separate = ','
  if (isStringNullOrEmpty(value)) {
      return ''
  }
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separate)
}

export function formatSize(width, size) {
  let textSize = width * size / 450
  return parseInt(textSize)
}

export function formatTextSize(width, size) {
  let textSize = width * size / 450
  return textSize
}

export function scale(size) {
  let width = Dimensions.get('window').width
  let textSize = width * size / 375 // 375 is width default of iphone 
  return textSize
}



