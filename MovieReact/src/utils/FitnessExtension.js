import { NativeModules } from "react-native";

const getSteps = ({ startDate, endDate, interval = "days", includeManuallyAdded = false }) =>
    NativeModules.RNFitness.getSteps(
    parseDate(startDate),
    parseDate(endDate),
	interval,
	includeManuallyAdded,
  );

const parseDate = date => {
	if (!date) {
	  throw Error("Date not valid");
	}
	const parsed = Date.parse(date);
  
	if (Number.isNaN(parsed)) {
	  throw Error("Date not valid");
	}
	return parsed;
};

export default {
    ...NativeModules.RNFitness,
    getSteps,
}