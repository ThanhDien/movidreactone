

const IMAGE = {
    ICON_LIKE :   require('../../assets/images/ic_like_orange.png'),
    ICON_UNLIKE : require('../../assets/images/ic_like.png'),
    ICON_BACKGROUND : require('../../assets/images/bg.png'),
    ICON_BACK: require('../../assets/images/back.png'),
    ICON_LOGOUT: require('../../assets/images/logOut.png'),
    ICON_FACEBOOK: require('../../assets/images/ic_Facebook.png'),
}

export {IMAGE}