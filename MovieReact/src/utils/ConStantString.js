const StringConstant = {
    login : 'Đăng Nhập',
    forPass:'Quên mặt khẩu?',
    notAcc :'Bạn chưa có tài khoảng?',
    reg:'Đăng ký',
    pass: 'Mật khẩu',
    email: 'Email',
    hoten:'Họ Tên',
    comfirmPass: 'Xác nhận mật khẩu',
    rules:'Bằng việc chọn vào nút Đăng ký, bạn đã đồng ý với?',
    titleForPass: 'Hãy nhập email bạn đã dùng để tạo tài khoản',
    resentPass: 'Gửi lại mật khẩu',
 }
 
 export {StringConstant}