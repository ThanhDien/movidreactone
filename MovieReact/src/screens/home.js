import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ImageBackground,
  TextInput,
  Alert,
  Keyboard,
  SafeAreaView,
  TouchableWithoutFeedback,
  Linking,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Button from 'react-native-button';

import * as Utils from '../utils/Utils';
import Login from './Login';

import {getVideoFromServer, logOutFromServer} from '../../networking/Server';
import {DetailMovieScreen, LoginScreen, MapGGScreen} from '../../ScreensName';
import {IMAGE} from '../utils/Constants';
// import {styles} from '../theme/ApplicationStyles';
import {StringConstant} from '../utils/ConStantString';

import {getISLOGIN, getLOGOut, getDataUser} from '../../data/userDefault';
import mapGG from './mapGG';

export default class home extends Component {
  //custom navi
  static navigationOptions = ({navigation}) => {

    const { params = {} } = navigation.state;

    let headerTitle = 'HFILM';
    let headerTitleStyle = {color: '#fff'};
    let headerStyle = {backgroundColor: 'rgba(255,77,77,0.9)'};
    let headerRight = (
      <Button
        containerStyle={{
          margin: 5,
          padding: 10,
          borderRadius: 10,
          backgroundColor: '#fff',
        }}
        style={{fontSize: 15, color: 'black',}}
        onPress={() => {
          params.onMap();
        }}>
        Google map
      </Button>
    );

    return {headerTitle, headerTitleStyle, headerStyle, headerRight};
  };

  _onMap() {
    console.log('onmap');
    this.props.navigation.navigate(MapGGScreen);
  }

  constructor(properties) {
    super(properties);
    this.state = {
      videoFromServer: [],
      loginData: this.props.navigation.state.params,
      page: 1,
      isLoading: false,
      totalPage: 1,
    };
  }

  componentDidMount() {
    //set _onMap
    this.props.navigation.setParams({onMap: this._onMap.bind(this)});

    this.setState({isLoading: true}, this.refreshFromServer(this.state.page));

    getISLOGIN('ISLOGIN').then(value => {
      console.log('home kakaka', value);
    });
  }

  // callAPI
  refreshFromServer = page => {
    getVideoFromServer(page)
      .then(jsonData => {
        this.setState({
          videoFromServer: this.state.videoFromServer.concat(jsonData.data),
          totalPage: jsonData.paging['total_pages'],
        });
        this.setState({refreshing: false, isLoading: false});
      })
      .catch(error => {
        this.setState({videoFromServer: []});
        console.log(`Error is Get Video ${error}`);
        this.setState({isLoading: false});
      });
  };

  handleLoadMoreVideo = () => {
    const {page, totalPage} = this.state;
    if (page <= totalPage) {
      this.setState(
        {page: this.state.page + 1, isLoading: true},
        this.refreshFromServer(this.state.page),
      );
    }
  };

  renderFooter = () => {
    return this.state.isLoading ? (
      <View style={styles.load}>
        <ActivityIndicator size="large" />
      </View>
    ) : null;
  };

  onForgotpassword() {
    const {Email} = this.state;
    Alert.alert('Credentials', ` ${Email}`);
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={IMAGE.ICON_BACKGROUND}
          style={styles.backgroundImage}>
          <SafeAreaView style={styles.safe}>
            <Text>{this.props.navigation.state.name}</Text>
            <FlatList
              style={{marginTop: 10}}
              data={this.state.videoFromServer}
              renderItem={({item, index}) => {
                return (
                  <FlatListItem
                    navi={this.props.navigation}
                    item={item}
                    index={index}></FlatListItem>
                );
              }}
              keyExtractor={(item, index) => index.toString()}
              onEndReached={this.handleLoadMoreVideo}
              onEndReachedThreshold={0}
              ListFooterComponent={this.renderFooter()}></FlatList>
          </SafeAreaView>
        </ImageBackground>
      </View>
    );
  }
}

class FlatListItem extends Component {
  constructor(properties) {
    super(properties);

    const titleStringTitle = this.props.item.title.split('/ ');

    this.state = {
      titleSud: titleStringTitle[1],
      titleEN: titleStringTitle[0],
      isLike: false,
      textLike: 'Thích',
    };
  }
  actionMovingDetail = () => {
    this.props.navi.navigate(DetailMovieScreen, this.props.item);
  };

  islikeAction() {
    this.setState(previousState => {
      return {
        isLike: !previousState.isLike,
        textLike: this.state.isLike ? 'Thích' : 'Đã thích',
      };
    });
  }

  render() {
    return (
      <View key={this.props.index} style={styles.flatListView}>
        <View style={{backgroundColor: 'transparent', flex: 1}}>
          <Image
            source={{uri: this.props.item.image}}
            style={styles.backgroundImage}
          />
        </View>
        <View style={{flex: 2, padding: 10}}>
          <Text style={styles.tyleCellText}>{this.state.titleEN}</Text>
          <Text style={styles.tyleCellText} numberOfLines={1}>
            {this.state.titleSud}
          </Text>
          <Text style={styles.textSeeView}>
            {'Lượt Xem: ' + this.props.item.views}
          </Text>
          <Text
            numberOfLines={5}
            style={{color: 'white', margin: 4, fontSize: 13}}>
            {this.props.item.description}
          </Text>

          <View style={styles.roomButton}>
            <TouchableOpacity
              onPress={() => {
                this.islikeAction();
              }}
              style={styles.buttonLikeUser}>
              <Image
                source={this.state.isLike ? IMAGE.ICON_LIKE : IMAGE.ICON_UNLIKE}
                style={styles.imgelikeType}
              />
              <Text
                style={
                  this.state.isLike ? styles.likeStyle : styles.UnlikeStyle
                }>
                {this.state.textLike}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.actionMovingDetail();
              }}
              style={styles.buttonList}>
              <Text> Xem phim </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundImage: {
    resizeMode: 'cover',
    height: '100%',
    width: '100%',
  },
  title: {
    color: '#fff',
    fontSize: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  typeLogin: {
    width: '100%',
    // height: 350,
    borderTopColor: 'gray',
    borderTopWidth: 1,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  textType: {
    margin: 15,
    padding: 10,
    borderRadius: 15,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    width: '80%',
    height: 40,
    fontSize: 17,
    backgroundColor: 'transparent',
    color: '#fff',
  },
  ButtonType: {
    flex: 1,
    borderRadius: 15,
  },
  textTitle: {
    marginBottom: 20,
    color: '#fff',
    fontSize: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonsTouchable: {
    backgroundColor: '#ff8000',
    marginTop: 20,
    borderRadius: 10,
    width: '80%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
  safe: {
    backgroundColor: 'transparent',
    width: '100%',
    height: '100%',
  },
  viewBottom: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: 250,
    width: '100%',
    backgroundColor: 'transparent',
  },
  textRes: {
    color: '#ff8000',
    fontSize: 14,
  },
  textBottom: {
    fontSize: 14,
    color: '#fff',
    margin: 10,
  },
  touchS: {
    marginBottom: 0,
    alignContent: 'flex-end',
    justifyContent: 'flex-end',
  },
  viewrules: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'transparent',
    marginBottom: 10,
    flexDirection: 'row',
  },
  titleRule: {
    fontSize: 14,
    color: '#ff8000',
  },
  flatListItem: {
    color: 'white',
    padding: 10,
    fontSize: 16,
  },
  flatListView: {
    backgroundColor: 'rgba(255, 235, 230, 0.2)',
    flex: 1,
    margin: 10,
    height: 230,
    marginBottom: 10,
    marginTop: 0,
    borderRadius: 10,
    flexDirection: 'row',
    overflow: 'hidden',
  },
  tyleCellText: {
    color: 'white',
    margin: 4,
  },
  buttonList: {
    width: '45%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ff8000',
    padding: 10,
    borderRadius: 10,
  },
  buttonListLeft: {
    width: '45%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor:'yellow',
    padding: 10,
    borderRadius: 10,
  },
  buttonLikeUser: {
    width: '45%',
    height: 50,
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
    flexDirection: 'row',
  },
  likeStyle: {
    color: '#ff8000',
    fontSize: 14,
  },
  UnlikeStyle: {
    color: '#fff',
    fontSize: 14,
  },
  load: {
    margin: 10,
    alignItems: 'center',
  },
  textSeeView: {
    margin: 2,
    fontSize: 13,
    color: '#ff8000',
    fontStyle: 'italic',
  },
  roomButton: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 60,
  },
  imgelikeType: {
    height: 20,
    width: 20,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginRight: 5,
    resizeMode: 'cover',
  },
});
