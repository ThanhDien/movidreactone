import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {
  showNotification,
  handleScheduleNotification,
  handleCancel,
} from './notification.ios';

export default class TestPushIos extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() =>
            showNotification('Hello', 'Ở nhà ăn trứng chiên, Siêu nhân điền')
          }
          style={styles.buttonStyle}>
          <Text style={styles.textStyle}>Click here push</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            handleScheduleNotification(
              'Hello',
              'Đã 5 giây qua, rồi vẫn ăn trứng chiên',
            )
          }
          style={styles.buttonStyle}>
          <Text style={styles.textStyle}>Click here push after 5 secon</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleCancel()}
          style={styles.buttonStyle}>
          <Text style={styles.textStyle}>Cancel all notify</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  buttonStyle: {
    backgroundColor: 'blue',
    height: 50,
    width: 250,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    margin: 10,
    padding: 10,
  },
  textStyle: {
    color: '#fff',
  },
});
