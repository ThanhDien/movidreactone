import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ImageBackground,
  TextInput,
  Button,
  Alert,
  Keyboard,
  SafeAreaView,
  TouchableWithoutFeedback,
  Linking,
  KeyboardAvoidingView,
} from 'react-native';

import * as Utils from '../utils/Utils';
import Login from './Login';
import {registryFromServer} from '../../networking/Server';
import {HomeScreen} from '../../ScreensName';
import {setStoreDataUser} from '../../data/userDefault';

import {styles} from '../theme/ApplicationStyles';
import {StringConstant} from '../utils/ConStantString';

export default class registered extends Component {
  constructor(properties) {
    super(properties);
    this.state = {
      UserName: '',
      Email: '',
      password: '',
      confirmPasword: '',
      dataUser: {},
    };
  }
  onRegister(par) {
    const {UserName, Email, password, confirmPasword} = this.state;
    if (
      Email.length === 0 ||
      password.length === 0 ||
      UserName.length === 0 ||
      confirmPasword.length === 0
    ) {
      Alert.alert('Thông Báo', 'Bạn Không thể để trống');
    } else {
      registryFromServer(par)
        .then(data => {
          if (data.error !== true) {
            setStoreDataUser('accToken', this.state.dataUser.access_token);
            setStoreDataUser('ISLOGIN', JSON.stringify(true));
            this.props.navigation.replace(HomeScreen), {};
          } else if (data['message'] !== null) {
            Alert.alert('Thông Báo', JSON.stringify(data['message']));
          }
        })
        .catch(error => {
          console.log(`Error is Get registryFromServer ${error}`);
        });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require('../../assets/images/bg.png')}
          style={styles.backgroundImage}>
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
            accessible={true}>
            <SafeAreaView style={styles.safe}>
              <Text style={styles.textTitle}>Đăng Ký</Text>
              <KeyboardAvoidingView style={styles.safe} behavior="padding">
                <View style={styles.typeLogin}>
                  <TextInput
                    autoFocus={true}
                    keyboardType="name-phone-pad"
                    autoCorrect={false}
                    returnKeyType="next"
                    value={this.state.UserName}
                    onChangeText={UserName => this.setState({UserName})}
                    style={styles.textType}
                    placeholderTextColor="#fff"
                    placeholder={StringConstant.hoten}
                    onSubmitEditing={() => {
                      this.refs.txtEmail.focus();
                    }}
                  />
                  <TextInput
                    ref={'txtEmail'}
                    returnKeyType="next"
                    keyboardType="email-address"
                    autoCorrect={false}
                    value={this.state.Email}
                    onChangeText={Email => this.setState({Email})}
                    style={styles.textType}
                    placeholderTextColor="#fff"
                    placeholder={StringConstant.email}
                    onSubmitEditing={() => {
                      this.refs.txtPassword.focus();
                    }}
                  />
                  <TextInput
                    ref={'txtPassword'}
                    returnKeyType="next"
                    autoCorrect={false}
                    color="#fff"
                    placeholderTextColor="#fff"
                    value={this.state.password}
                    onChangeText={password => this.setState({password})}
                    label="Password"
                    secureTextEntry={true}
                    style={styles.textType}
                    placeholder={StringConstant.pass}
                    onSubmitEditing={() => {
                      this.refs.txtConfirmPasword.focus();
                    }}
                  />
                  <TextInput
                    ref={'txtConfirmPasword'}
                    returnKeyType="go"
                    autoCorrect={false}
                    value={this.state.confirmPasword}
                    onChangeText={confirmPasword =>
                      this.setState({confirmPasword})
                    }
                    style={styles.textType}
                    placeholderTextColor="#fff"
                    secureTextEntry={true}
                    placeholder={StringConstant.comfirmPass}
                  />
                  <TouchableOpacity
                    onPress={() => {
                      let params = {
                        full_name: this.state.UserName,
                        email: this.state.Email,
                        UserName: this.state.password,
                      };
                      if (this.state.password !== this.state.confirmPasword) {
                        Alert.alert(
                          'Thông Báo',
                          'Xác nhận mật khẩu không giống',
                        );
                      } else {
                        this.onRegister(params);
                      }
                    }}
                    style={styles.buttonsTouchable}>
                    <Text style={styles.title}>{StringConstant.reg}</Text>
                  </TouchableOpacity>
                </View>
              </KeyboardAvoidingView>

              <View style={styles.viewBottom}>
                <Text style={styles.textBottom}>{StringConstant.rules}</Text>
                <View style={styles.viewrules}>
                  <TouchableOpacity
                    onPress={() => {
                      Linking.openURL(
                        'https://www.youtube.com/watch?v=fOCGMGNqHKU',
                      );
                    }}>
                    <Text style={styles.titleRule}>Điền khoảng sử dụng</Text>
                  </TouchableOpacity>
                  <Text style={{color: '#fff', fontSize: 14}}> và </Text>
                  <TouchableOpacity
                    onPress={() => {
                      Linking.openURL(
                        'https://translate.google.com/?hl=vi&tab=TT',
                      );
                    }}>
                    <Text style={styles.titleRule}>quy định bảo mật</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </SafeAreaView>
          </TouchableWithoutFeedback>
        </ImageBackground>
      </View>
    );
  }
}