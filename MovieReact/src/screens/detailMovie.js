import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ImageBackground,
  TextInput,
  Button,
  Alert,
  Keyboard,
  SafeAreaView,
  TouchableWithoutFeedback,
  Linking,
  useState,
} from 'react-native';

import * as Utils from '../utils/Utils';
import Login from './Login';

import {WebView} from 'react-native-webview';
import {IMAGE} from '../utils/Constants';
import {getDataUser, getLOGOut} from '../../data/userDefault';
import {logOutFromServer} from '../../networking/Server';
import {LoginScreen} from '../../ScreensName';

export default class detailMovie extends Component {
  constructor(properties) {
    super(properties);
    this.dataObject = this.props.navigation.state.params;
    const titleStringTitle = this.dataObject.title.split('/ ');
    const Video = this.dataObject.link;
    const MyVideo = Video.split('watch?v=');
    const EmbededVideo = MyVideo.join('embed/');

    this.state = {
      titleSud: titleStringTitle[1],
      titleEN: titleStringTitle[0],
      Video: EmbededVideo,
      showText: false,
      isLike: false,
      textLike: 'Thích',
      accToken: '',
    };
  }

  componentDidMount() {
    getDataUser('accToken').then(value => {
      this.setState({
        accToken: value,
      });
    });
  }

  onLogOut = () => {
    logOutFromServer(this.state.accToken)
      .then(value => {
        this.clearDataAll();
      })
      .catch(error => {
        console.log(`Error is logOutFromServer ${error}`);
      });
  };
  //ClearData
  clearDataAll() {
    getLOGOut('ISLOGIN').then(value => {
      console.log('xoá data ISLOGIN', value);
      if (value === true) {
        this.props.navigation.replace(LoginScreen);
      }
    });

    getLOGOut('accToken').then(value => {
      console.log('xoá data accToken', value);
    });
  }

  islikeAction() {
    this.setState(previousState => {
      return {
        isLike: !previousState.isLike,
        textLike: this.state.isLike ? 'Thích' : 'Đã thích',
      };
    });
  }

  showTitle() {
    this.setState(previousState => {
      return {
        showText: !previousState.showText,
      };
    });
  }

  onForgotpassword() {
    const {Email} = this.state;
    Alert.alert('Credentials', ` ${Email}`);
  }

  render() {
    return (
      <ImageBackground
        source={IMAGE.ICON_BACKGROUND}
        style={styles.backgroundImage}>
        <View style={styles.viewContent}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack();
            }}
            style={styles.headerStyleBack}>
            <Image source={IMAGE.ICON_BACK} style={styles.imageLike} />
          </TouchableOpacity>
          <View style={styles.headerStyle}>
            <Text style={styles.titleHeader}>{this.state.titleEN}</Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              Alert.alert('Thông Báo', 'Bạn có muốn đăng xuất', [
                {
                  text: 'Đồng ý',
                  onPress: () => this.onLogOut(),
                  style: 'cancel',
                },
                {text: 'Không', onPress: () => console.log('OK Pressed')},
              ]);
            }}
            style={styles.headerStyleLogOut}>
            <Image source={IMAGE.ICON_LOGOUT} style={styles.imageLike} />
          </TouchableOpacity>
        </View>
        <SafeAreaView style={styles.container}>
          <View style={styles.viewDetail}>
            <View
              style={{
                backgroundColor: 'transparent',
                flex: 2,
                flexDirection: 'row',
              }}>
              <View style={{flex: 1, backgroundColor: 'transparent'}}>
                <Image
                  source={{uri: this.dataObject.image}}
                  style={styles.logoImage}
                />
              </View>
              <View
                style={{flex: 2, backgroundColor: 'transparent', margin: 5}}>
                <Text style={styles.typeSud}>{this.state.titleSud}</Text>

                <Text style={styles.titleView}>
                  Lượt xem: {this.dataObject.views}
                </Text>
                <Text style={styles.titleText}>
                  <Text style={{fontWeight: 'bold'}}>Genres: </Text>
                  {this.dataObject.category}
                </Text>
                <Text style={styles.titleText}>
                  <Text style={{fontWeight: 'bold'}}>Actor: </Text>
                  {this.dataObject.actor}
                </Text>
                <Text style={styles.titleText}>
                  <Text style={{fontWeight: 'bold'}}>Director: </Text>
                  {this.dataObject.director}
                </Text>
                <Text style={styles.titleText}>
                  <Text style={{fontWeight: 'bold'}}>Manufacturer: </Text>
                  {this.dataObject.manufacturer}
                </Text>
                <Text style={styles.titleText}>
                  <Text style={{fontWeight: 'bold'}}>Thời lượng phim: </Text>
                  {this.dataObject.duration} minute
                </Text>

                <TouchableOpacity
                  onPress={() => {
                    this.islikeAction();
                  }}
                  style={styles.buttonLikeUser}>
                  <Image
                    source={
                      this.state.isLike ? IMAGE.ICON_LIKE : IMAGE.ICON_UNLIKE
                    }
                    style={styles.imageLikeType}
                  />
                  <Text
                    style={
                      this.state.isLike ? styles.likeStyle : styles.UnlikeStyle
                    }>
                    {this.state.textLike}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
              <Text
                style={{color: '#fff', padding: 10}}
                numberOfLines={this.state.showText ? 0 : 4}
                ellipsizeMode="tail">
                {this.dataObject.description}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.showTitle();
                }}
                style={styles.typeShowMore}>
                <Text style={styles.titleMoreSee}>Xem thêm</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{marginTop: 20, height: '50%'}}>
            <Text style={styles.titleTrailer}> XEM TRAILLER</Text>
            <WebView
              style={{flex: 1}}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              source={{uri: this.state.Video}}
            />
          </View>
        </SafeAreaView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    resizeMode: 'cover',
    flex: 1,
  },
  titleView: {
    margin: 4,
    fontSize: 13,
    color: '#ff8000',
    fontStyle: 'italic',
    fontSize: 14,
  },
  titleText: {
    fontSize: 14,
    color: '#fff',
    margin: 3,
    backgroundColor: 'transparent',
  },
  titleMoreSee: {
    fontSize: 15,
    fontStyle: 'italic',
    margin: 10,
    color: '#ff8000',
    textDecorationLine: 'underline',
  },
  titleTrailer: {
    fontSize: 15,
    fontWeight: 'bold',
    margin: 10,
    color: '#ff8000',
  },
  buttonLikeUser: {
    marginVertical: 10,
    flexDirection: 'row',
  },
  likeStyle: {
    color: '#ff8000',
    fontSize: 14,
  },
  UnlikeStyle: {
    color: '#fff',
    fontSize: 14,
  },
  headerStyleBack: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flex: 1,
    marginTop: 25,
    marginLeft: 15,
  },
  headerStyle: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 2,
    marginTop: 25,
    // backgroundColor:'blue'
  },
  headerStyleLogOut: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop: 25,
  },
  titleHeader: {
    fontSize: 16,
    color: '#fff',
    fontWeight: 'bold',
  },
  viewContent: {
    backgroundColor: 'rgba(255,77,77,0.9)',
    height: 90,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageLike: {
    width: 30,
    height: 30,
  },
  viewDetail: {height: '50%', backgroundColor: 'transparent'},
  logoImage: {
    flex: 1,
    resizeMode: 'cover',
    margin: 5,
    backgroundColor: 'transparent',
    borderColor: '#fff',
    borderWidth: 1,
  },
  typeSud: {
    fontSize: 15,
    color: '#fff',
    margin: 5,
    fontWeight: 'bold',
  },
  imageLikeType: {
    height: 20,
    width: 20,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginRight: 5,
    resizeMode: 'cover',
  },
  typeShowMore: {
    width: '100%',
    height: 40,
    alignItems: 'flex-end',
  },
});
