import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ImageBackground,
  TextInput,
  Button,
  Alert,
  Keyboard,
  SafeAreaView,
  TouchableWithoutFeedback,
  Linking,
  KeyboardAvoidingView,
} from 'react-native';

import * as Utils from '../utils/Utils';
import Login from './Login';
import {color} from 'react-native-reanimated';

import {forPassFromServer} from '../../networking/Server';
import {HomeScreen,TestPushIosScreen} from '../../ScreensName';
import {StringConstant} from '../utils/ConStantString';
import {IMAGE} from '../utils/Constants';

import {
  showNotification,
  handleScheduleNotification,
  handleCancel,
} from './notification.ios';

export default class Forgotpassword extends Component {
  constructor(properties) {
    super(properties);
    this.state = {
      Email: '',
    };
  }

  onForgotpassword = () => {
    const {Email} = this.state;
    forPassFromServer(Email)
      .then(data => {
        if (data['error'] !== true) {
          this.props.navigation.replace(HomeScreen);
        } else if (data['message'] !== null) {
          Alert.alert('Thông Báo', JSON.stringify(data['message']));
        }
      })
      .catch(error => {
        Alert.alert('Thông Báo', 'SMTP Error: Could not authenticate.');
      });
  };

  render() {
    return (
      <View style={styles.containerForpass}>
        <ImageBackground
          source={IMAGE.ICON_BACKGROUND}
          style={styles.backgroundImage}>
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
            accessible={true}>
              
            <KeyboardAvoidingView behavior="padding">
              <SafeAreaView style={styles.safe}>
                <TouchableOpacity
                  onPress={() =>
                 {this.props.navigation.replace(TestPushIosScreen);}
                  }>
                  <Text style={styles.textTitle}>{StringConstant.forPass}</Text>
                </TouchableOpacity>

                <View style={styles.typeLogin}>
                  <View style={{height: 100}}>
                    <Text
                      style={{
                        color: '#fff',
                        fontSize: 20,
                        alignContent: 'center',
                        justifyContent: 'center',
                        margin: 25,
                      }}>
                      {StringConstant.titleForPass}
                    </Text>
                  </View>
                  <TextInput
                    keyboardType="email-address"
                    autoCorrect={false}
                    returnKeyType="next"
                    value={this.state.Email}
                    onChangeText={Email => this.setState({Email})}
                    style={styles.textType}
                    placeholderTextColor="#fff"
                    placeholder={StringConstant.email}
                  />
                  <TouchableOpacity
                    onPress={() => {
                      if (this.state.Email.length !== 0) {
                        this.onForgotpassword();
                      } else {
                        Alert.alert('Thông Báo', 'Email Không được bỏ trống');
                      }
                    }}
                    style={styles.buttonsTouchable}>
                    <Text style={styles.title}>
                      {StringConstant.resentPass}
                    </Text>
                  </TouchableOpacity>
                </View>
              </SafeAreaView>
            </KeyboardAvoidingView>
          </TouchableWithoutFeedback>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    resizeMode: 'cover',
    height: '100%',
    width: '100%',
  },
  title: {
    color: '#fff',
    fontSize: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  typeLogin: {
    width: '100%',
    // height: 350,
    borderTopColor: 'gray',
    borderTopWidth: 1,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  textType: {
    margin: 15,
    padding: 10,
    borderRadius: 15,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    width: '80%',
    height: 40,
    fontSize: 17,
    backgroundColor: 'transparent',
    color: '#fff',
  },
  ButtonType: {
    flex: 1,
    borderRadius: 15,
  },
  textTitle: {
    marginBottom: 20,
    color: '#fff',
    fontSize: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonsTouchable: {
    backgroundColor: '#ff8000',
    marginTop: 20,
    borderRadius: 10,
    width: '80%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
  safe: {
    backgroundColor: 'transparent',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewBottom: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: 250,
    width: '100%',
    backgroundColor: 'transparent',
  },
  textRes: {
    color: '#ff8000',
    fontSize: 14,
  },
  textBottom: {
    fontSize: 14,
    color: '#fff',
    margin: 10,
  },
  touchS: {
    marginBottom: 0,
    alignContent: 'flex-end',
    justifyContent: 'flex-end',
  },
  viewrules: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'transparent',
    marginBottom: 10,
    flexDirection: 'row',
  },
  titleRule: {
    fontSize: 14,
    color: '#ff8000',
  },
});
