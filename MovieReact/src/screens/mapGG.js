import React, {Component} from 'react';
import {Text, StyleSheet, View, Platform, Dimensions} from 'react-native';
import MapView, {Callout, Circle, Marker} from 'react-native-maps';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import Geolocation from '@react-native-community/geolocation';

export default class mapGG extends Component {
  constructor(properties) {
    super(properties);
    this.state = {
      latitude: 0,
      longitude: 0,
      error: null,
    };
  }

  componentDidMount() {
    Geolocation.getCurrentPosition(
      position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      error => this.setState({error: error.message}),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 2000},
    );
  }

  render() {
    return (
      <View style={styles.container}>
        {/*
         // nào có tiền nạp vào key dùng được 
        
        <GooglePlacesAutocomplete
          placeholder="Search"
          backgroundColor="red"
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true
            console.log(data, details);
          }}
          query={{
            key: 'AIzaSyDi7xb044lMuFd5jP9g1QUuMWRnM_rzClA',
            language: 'en',
          }}
          style={{
            container: {
              flex: 0,
              position: 'absolute',
              width: '100%',
              zIndex: 1,
            },
            listView: {backgroundColor: 'white'},
          }}
        /> */}

        <MapView
          style={styles.map}
          region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: 0.005,
            longitudeDelta: 0.005,
          }}>
          <Marker
            coordinate={{
              latitude: this.state.latitude,
              longitude: this.state.longitude,
            }}
            title="Dương thanh điền"
            description="xin chào"
            pinColor="blue"
            draggable={true}
            onDragStart={e => {
              console.log('Drag start', e.nativeEvent.coordinate);
            }}
            onDragEnd={e => {
              this.setState({
                latitude: e.nativeEvent.coordinate.latitude,
                longitude: e.nativeEvent.coordinate.longitude,
              });
            }}>
            <Callout>
              <Text>Tôi ở đây nè !</Text>
            </Callout>
          </Marker>
          <Circle
            center={{
              latitude: this.state.latitude,
              longitude: this.state.longitude,
            }}
            radius={100}
          />
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
