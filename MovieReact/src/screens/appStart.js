import LoginComponent from './Login';
import HomeComponent from './home';
import RegisteredComponent from './registered';
import ForgotpasswordComponent from './Forgotpassword';
import DetailMovieComponent from './detailMovie';
import TestPushIosComponent from './TestPushIos'

import MapGGComponent from './mapGG';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import AsyncStorage from "@react-native-async-storage/async-storage"
import { getISLOGIN } from '../../data/userDefault';

import {
  AppRegistry,
  SectionList,
  StyleSheet,
  Alert,
  Platform,
  Text,
  View,
  Button,
} from 'react-native';

let isCheck = false

export default createAppContainer(

createStackNavigator( 
  {
    LoginComponent: {
      screen: LoginComponent,
      navigationOptions: {
        headerShown: false,
      },
    },
    HomeComponent: {
      screen: HomeComponent,
    },
    RegisteredComponent: {
      screen: RegisteredComponent,
      navigationOptions: {
        headerShown: false,
      },
    },
    ForgotpasswordComponent: {
      screen: ForgotpasswordComponent,
      navigationOptions: {
        headerShown: false,
      },
    },
    DetailMovieComponent: {
      screen: DetailMovieComponent,
      navigationOptions: {
        headerShown: false
      },
    },
    MapGGComponent: {
      screen: MapGGComponent,
      navigationOptions: {
        headerShown: true,
      },
    },
    TestPushIosComponent: {
      screen: TestPushIosComponent,
      navigationOptions: {
        headerShown: true,
      },
    },
  },
  {
    initialRouteName: isCheck ? 'HomeComponent' : 'LoginComponent',
  },
));


