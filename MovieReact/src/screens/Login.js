import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ImageBackground,
  TextInput,
  Button,
  Alert,
  Keyboard,
  SafeAreaView,
  TouchableWithoutFeedback,
  useState,
  KeyboardAvoidingView,
} from 'react-native';

import * as Utils from '../utils/Utils';
import {loginFromServer, loginFromServerSocial} from '../../networking/Server';
import {Home} from './appStart';
import {
  HomeScreen,
  RegisteredScreen,
  ForgotpasswordScreen,
  MapGGScreen,
} from '../../ScreensName';
import {setStoreDataUser, getISLOGIN, getLOGOut} from '../../data/userDefault';
import {styles} from '../theme/ApplicationStyles';
import {IMAGE} from '../utils/Constants';
import {StringConstant} from '../utils/ConStantString';
import {LoginManager} from 'react-native-fbsdk-next';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';

export default class Login extends Component {
  constructor(properties) {
    super(properties);
    this.state = {
      Email: '',
      password: '',
      dataUser: {},
      userInfo: {},
    };
  }

  // callAPILogin
  loginUser(params) {
    const {Email, password} = this.state;

    if (Email.length === 0 || password.length === 0) {
      Alert.alert('Thông Báo', 'Bạn Không thể để trống');
    } else {
      loginFromServer(params)
        .then(data => {
          if (data['error'] !== true) {
            this.setState({dataUser: data.data});
            let dataScreen = {
              name: this.state.dataUser.access_token,
              emai: 'Test Du lieu',
            };
            setStoreDataUser('accToken', this.state.dataUser.access_token);
            setStoreDataUser('ISLOGIN', JSON.stringify(true));

            console.log('du lieu truyen', this.state.dataUser.access_token);
            this.props.navigation.replace(HomeScreen, dataScreen);
          } else if (data['message'] !== null) {
            Alert.alert('Thông Báo', JSON.stringify(data['message']));
          }
        })
        .catch(error => {
          console.log(`Error loginUser 123  ${error}`);
        });
    }
  }

  loginFB = () => {
    console.log(` login FB`);
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function (result) {
        if (result.isCancelled) {
          alert('Login Cancelled ' + JSON.stringify(result));
        } else {
          alert(
            'Login success with  permisssions: ' +
              result.grantedPermissions.toString(),
          );
          alert('Login Success ' + result.toString());
          console.log('data của facebook', JSON.stringify(result));
        }
      },
      function (error) {
        alert('Login failed with error: ' + error);
      },
    );
  };

  loginSocialNetworking(params){
    console.log('loginSocialNetworking', userSocial);
    loginFromServerSocial(params)
      .then(data => {
        if (data['error'] !== true) {
          this.setState({dataUser: data.data});
          setStoreDataUser('accToken', this.state.dataUser.access_token);
          setStoreDataUser('ISLOGIN', JSON.stringify(true));

         
          this.props.navigation.replace(HomeScreen, dataScreen);
        } else if (data['message'] !== null) {
          Alert.alert('Thông Báo', JSON.stringify(data['message']));
        }
      })
      .catch(error => {
        console.log(`Error loginFromServerSocial   ${error}`);
      });
  };

  loginGG = () => {
    GoogleSignin.configure({
      // androidClientId: 'ADD_YOUR_ANDROID_CLIENT_ID_HERE',
      iosClientId: '975375884132-qeosq4g6e1qs4dphs7ah5v9oha2ueqjg.apps.googleusercontent.com',
    });
    async function signIn() {
      try {
        await GoogleSignin.hasPlayServices();
        const userInfo = await GoogleSignin.signIn();
        //If login is successful you'll get user info object in userInfo below I'm just printing it to console. You can store this object in a usestate or use it as you like user is logged in.
        console.log('Dũ liệu gg',userInfo);
        let userSocial = {
          email: userInfo.user.name,
          password: userInfo.user.email,
        };
        
        // this.setState({ userInfo: userInfo });
        this.show()
      } catch (error) {
        if (error.code === statusCodes.SIGN_IN_CANCELLED) {
          alert('You cancelled the sign in.');
        } else if (error.code === statusCodes.IN_PROGRESS) {
          console.log('Google sign In operation is in process');
        } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
          alert('Play Services not available');
        } else {
          console.log(
            'Something unknown went wrong with Google sign in. ' +
              error.message,
          );
        }
      }
    }
    signIn();
  };

  componentDidMount() {
    getISLOGIN('ISLOGIN').then(value => {
      console.log('ISLOGIN', value);
    });
  }

  show =()=>{
    Alert.alert('thong báo','có cái quan')
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={IMAGE.ICON_BACKGROUND}
          style={styles.backgroundImage}>
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
            accessible={true}>
            <SafeAreaView style={styles.safe}>
              <KeyboardAvoidingView style={styles.safe} behavior="padding">
                <Text style={styles.textTitle}>Đăng Nhập</Text>
                <View style={styles.typeLogin}>
                  <TextInput
                    autoFocus={true}
                    keyboardType="email-address"
                    autoCorrect={false}
                    returnKeyType="next"
                    value={this.state.Email}
                    onChangeText={Email => this.setState({Email})}
                    style={styles.textType}
                    placeholderTextColor="#fff"
                    placeholder={StringConstant.email}
                    onSubmitEditing={() => {
                      this.refs.txtPassWord.focus();
                    }}
                  />
                  <TextInput
                    color="#fff"
                    placeholderTextColor="#fff"
                    value={this.state.password}
                    onChangeText={password => this.setState({password})}
                    label="Password"
                    secureTextEntry={true}
                    style={styles.textType}
                    placeholder={StringConstant.pass}
                    autoCorrect={false}
                    returnKeyType="go"
                    ref={'txtPassWord'}
                  />
                  <TouchableOpacity
                    onPress={() => {
                      let params = {
                        email: this.state.Email, //,'a23@gmail.com'
                        password: this.state.password,
                      };
                      this.loginUser(params);
                    }}
                    style={styles.buttonsTouchable}>
                    <Text style={styles.title}>{StringConstant.login}</Text>
                  </TouchableOpacity>

                  <View style={styles.buttonsTouchableFacebook}>
                    <TouchableOpacity
                      onPress={() => {
                        // Alert.alert('thong bao', 'Facebook login');
                        // this.props.navigation.navigate(ForgotpasswordScreen);
                        //FB
                        this.loginFB();
                      }}
                      style={styles.buttonsTouchableFacebook}>
                      <Image
                        source={IMAGE.ICON_FACEBOOK}
                        style={{width: 30, height: 30}}
                      />

                      <Text style={styles.titleFacebook}>Login Facebook</Text>
                    </TouchableOpacity>
                  </View>

                  <View style={styles.buttonsTouchableGoogle}>
                    <TouchableOpacity
                      onPress={() => {
                        this.loginGG();
                      }}
                      style={styles.buttonsTouchableGoogle}>
                      <Text style={styles.titleFacebook}>Login Google</Text>
                    </TouchableOpacity>
                  </View>

                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate(ForgotpasswordScreen);
                    }}
                    style={styles.buttonsTouchable}>
                    <Text style={styles.title}>{StringConstant.forPass}</Text>
                  </TouchableOpacity>
                </View>
              </KeyboardAvoidingView>
              <View style={styles.viewBottomLogin}>
                <View style={{backgroundColor: 'blue'}}></View>
                <Text style={styles.textBottomLogin}>
                  {StringConstant.notAcc}
                </Text>
                <TouchableOpacity
                  style={styles.touchS}
                  onPress={() => {
                    this.props.navigation.navigate(RegisteredScreen);
                  }}>
                  <Text style={styles.textRes}>{StringConstant.reg}</Text>
                </TouchableOpacity>
              </View>
            </SafeAreaView>
          </TouchableWithoutFeedback>
        </ImageBackground>
      </View>
    );
  }
}
